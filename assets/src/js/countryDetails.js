let countriesData = JSON.parse(sessionStorage.getItem("obj"));
/*make url object*/
let url = window.location.href;
let urlObject = new URL(url);

/*get country name from the url made in app.js*/
getCountry();

function getCountry() {
    let countryName = new URLSearchParams(urlObject.search).get('country');
    countriesData.forEach((country) => {
        if (country.name.official.indexOf(countryName) !== -1) {
            cardDataToFill(country);
        }
    });
}

function cardDataToFill(countryDetails) {
    document.querySelector('.flag-country-details img').src = countryDetails.flags.png;
    document.querySelector('.country-name').innerText = countryDetails.name.official;
    document.querySelector('.native-name p').innerText = Object.values(countryDetails.name.nativeName)[0].official;
    document.querySelector('.population p').innerText = countryDetails.population;
    document.querySelector('.region p').innerText = countryDetails.region;
    document.querySelector('.sub-region p').innerText = countryDetails.subregion;
    document.querySelector('.capital p').innerText = countryDetails.capital[0];
    document.querySelector('.top-level-domain p').innerText = countryDetails.tld[0];
    document.querySelector('.currencies p').innerText = Object.values(countryDetails.currencies)[0].name;
   let language =  document.querySelector('.languages p').innerText = Object.values(countryDetails.languages).join(", ");

    /*getting all borders*/
    let borderArray = countryDetails.borders;

    /*make border buttons for border countries*/
    let buttons = createBorderButtons(borderArray);

    document.querySelector('.countries').innerHTML = buttons;
}

//create buttons with country border code
function createBorderButtons(borderArray) {
    let code = fetchBorderCode();
    let buttonsStr = "";
    if (borderArray) {  
        borderArray.forEach((border) => {
            buttonsStr += `<button type="button" class="border-btn btn mx-1 w-10">
              ${code[border]}
            </button>`;
        });
    }
    return buttonsStr;
}
//fetch the code of borders
function fetchBorderCode() {
    return (countryCode = countriesData.reduce((acc, ele) => {
        let cca3 = ele.cca3;
        let country = ele.name.common;
        if (!acc.cca3) {
            acc[cca3] = country;
        }
        return acc;
    }, {}));
}

//event listener on border buttons
let borderButtons = document.querySelector(".border-countries");
borderButtons.addEventListener("click", preddButton);

function preddButton(e) {
    let country = e.target.innerText;
    window.location.href = `?country=${country}`;
    getCountry();
}




