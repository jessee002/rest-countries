fetch(`https://restcountries.com/v3.1/all`)
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        sessionStorage.setItem("obj", JSON.stringify(data));
        createCards();
    })
    .catch((err) => {
        console.log(err);
    })
// console.log(countriesArr);

function createCards() {

    let countriesArr = JSON.parse(sessionStorage.getItem("obj"));
    let cardsContainer = document.querySelector('.cards');
    let cardsCollections = "";

    for (let i = 0; i < countriesArr.length; i++) {
        cardsCollections += `<div class="card">
        <img src="${countriesArr[i].flags.png}" alt="" />
        <div class="country-name-details">
        <div class="country-name">${countriesArr[i].name.official}</div>
        <div class="country-details">
            <div class="detail population">
                <span>Population:</span>
                <p>${countriesArr[i].population}</p>
            </div>
            <div class="detail region">
                <span>Region:</span>
                <p>${countriesArr[i].region}</p>
            </div>
                <div class="detail capital">
                <span>Capital:</span>
            <p>${countriesArr[i].capital}</p>
            </div>
        </div>
        </div>
        </div>`
    }
    /*putting cards into cardsContainer*/
    cardsContainer.innerHTML = cardsCollections;
}

/*search button*/

let searchInput = document.querySelector('.search-input');
searchInput.addEventListener('keyup', searchCountries);

let lastSelectedRegion = "";
let lastSelectedCountry = "";

function searchCountries(e) {
    let searchText = e.target.value.toLowerCase();
    let cards = document.querySelectorAll(".cards");
    lastSelectedCountry = searchText;

    Array.from(cards[0].children).forEach((card) => {
        let countryName = card.querySelector(".country-name").innerText.toLowerCase();
        let region = card.querySelector('.region').lastElementChild.innerText.toLowerCase();

        if (countryName.indexOf(searchText) != -1 && region.indexOf(lastSelectedRegion) != -1) {
            card.style.display = 'block';
        } else {
            card.style.display = 'none';
        }
    })
}
/*filter button*/
let filterData = document.querySelector('.filter');
filterData.addEventListener('click', filterByRegion);

function filterByRegion() {
    let selectedregion = document.querySelector('#filter').value.toLowerCase();
    lastSelectedRegion = selectedregion;
    let cards = document.querySelectorAll(".cards");

    Array.from(cards[0].children).forEach((card) => {
        let cardRegion = card.querySelector(".region").lastElementChild.innerText.toLowerCase();
        let country = card.querySelector('.country-name').innerText;
        console.log(country);
        if (((cardRegion.indexOf(selectedregion) != -1 && country.indexOf(lastSelectedCountry) != -1)) || selectedregion === "filter by region") {
            card.style.display = 'block';
        } else {
            card.style.display = 'none';
        }
    })
}

/*detail redirect page when clicked on card*/
let cards = document.querySelectorAll('.cards')[0];
cards.addEventListener('click', detailedCard)

function detailedCard(e) {
    let country = e.target.closest('.card').querySelector('.country-name').innerText;
    window.location.href = `/assets/src/html/countryDetails.html?country=${country}`;
}
